/*
 * Copyright (c) 2023 iSoftStone Information Technology (Group) Co.,Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { describe, beforeAll, beforeEach, afterEach, afterAll, it, expect } from "@ohos/hypium";
import router from '@system.router';
import CommonFunc from "./../../../../MainAbility/common/Common"
export default function AlignCenter_AddOneWidth() {
  describe('AlignCenter_AddOneWidth', function () {
    beforeEach(async function (done) {
      let options = {
        uri: 'MainAbility/pages/Flex/justifyContent/Center/AlignCenter_AddOneWidth',
      }
      try {
        router.clear();
        let pages = router.getState();
        console.info("get AlignCenter_AddOneWidth state success " + JSON.stringify(pages));
        if (!("AlignCenter_AddOneWidth" == pages.name)) {
          console.info("get AlignCenter_AddOneWidth state pages.name " + JSON.stringify(pages.name));
          let result = await router.push(options);
          await CommonFunc.sleep(2000);
          console.info("push AlignCenter_AddOneWidth page success " + JSON.stringify(result));
        }
      } catch (err) {
        console.error("push AlignCenter_AddOneWidth page error " + JSON.stringify(err));
        expect().assertFail();
      }
      done()
    });

    afterEach(async function () {
      await CommonFunc.sleep(2000);
      console.info("AlignCenter_ChangeOneWidth after each called");
    });

    /**
     * @tc.number    SUB_ACE_FLEX_JUSTIFYCONTENT_FLEXALIGN_CENTER_TEST_1300
     * @tc.name      testFlexAlignCenterTextWidth
     * @tc.desc      The interface is displayed when you change the width of the first subcomponent
     */
    it('testFlexAlignCenterTextWidth', 0, async function (done) {
      console.info('[testFlexAlignCenterTextWidth] START');
      globalThis.value.message.notify({name:'OneWidth', value:160})
      await CommonFunc.sleep(3000);
      let strJson = getInspectorByKey('Center_AddOneWidth_01');
      let obj = JSON.parse(strJson);
      expect(obj.$type).assertEqual('Flex');
      expect(obj.$attrs.constructor.direction).assertEqual('FlexDirection.Row');
      expect(obj.$attrs.constructor.justifyContent).assertEqual('FlexAlign.Center');
      let Center_AddOneWidth_011 = CommonFunc.getComponentRect('Center_AddOneWidth_011');
      let Center_AddOneWidth_012 = CommonFunc.getComponentRect('Center_AddOneWidth_012');
      let Center_AddOneWidth_013 = CommonFunc.getComponentRect('Center_AddOneWidth_013');
      let Center_AddOneWidth_01 = CommonFunc.getComponentRect('Center_AddOneWidth_01');
      expect(Center_AddOneWidth_011.top).assertEqual(Center_AddOneWidth_012.top);
      expect(Center_AddOneWidth_013.top).assertEqual(Center_AddOneWidth_012.top);
      expect(Center_AddOneWidth_011.top).assertEqual(Center_AddOneWidth_01.top);
      expect(Center_AddOneWidth_011.right).assertEqual(Center_AddOneWidth_012.left);
      expect(Center_AddOneWidth_012.right).assertEqual(Center_AddOneWidth_013.left);
      expect(Math.round(Center_AddOneWidth_011.left - Center_AddOneWidth_01.left)).assertEqual(vp2px(20));
      expect(Math.round(Center_AddOneWidth_01.right - Center_AddOneWidth_013.right)).assertEqual(vp2px(20));
      expect(Math.round(Center_AddOneWidth_011.right - Center_AddOneWidth_011.left)).assertEqual(vp2px(160));
      expect(Math.round(Center_AddOneWidth_012.right - Center_AddOneWidth_012.left)).assertEqual(vp2px(150));
      expect(Math.round(Center_AddOneWidth_013.right - Center_AddOneWidth_013.left)).assertEqual(vp2px(150));
      expect(Math.round(Center_AddOneWidth_011.bottom - Center_AddOneWidth_011.top)).assertEqual(vp2px(50));
      expect(Math.round(Center_AddOneWidth_012.bottom - Center_AddOneWidth_012.top)).assertEqual(vp2px(100));
      expect(Math.round(Center_AddOneWidth_013.bottom - Center_AddOneWidth_013.top)).assertEqual(vp2px(150));
      console.info('[testFlexAlignCenterTextWidth] END');
      done();
    });
  })
}

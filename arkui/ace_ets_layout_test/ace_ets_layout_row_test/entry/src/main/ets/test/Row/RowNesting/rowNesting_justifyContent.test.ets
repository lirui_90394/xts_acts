/**
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { describe, beforeAll, beforeEach, afterEach, afterAll, it, expect } from '@ohos/hypium';
import router from '@ohos.router';
import CommonFunc from '../../../MainAbility/common/Common';
import {MessageManager,Callback} from '../../../MainAbility/common/MessageManager';
export default function rowNesting_justifyContent() {
  describe('rowNesting_justifyContent', function () {
    beforeEach(async function (done) {
      console.info("rowNesting_justifyContent beforeEach start");
      let options = {
        url: "MainAbility/pages/Row/RowNesting/rowNesting_justifyContent",
      }
      try {
        router.clear();
        let pages = router.getState();
        console.info("get rowNesting_justifyContent state pages:" + JSON.stringify(pages));
        if (!("rowNesting_justifyContent" == pages.name)) {
          console.info("get rowNesting_justifyContent pages.name:" + JSON.stringify(pages.name));
          let result = await router.push(options);
          await CommonFunc.sleep(2000);
          console.info("push rowNesting_justifyContent page result:" + JSON.stringify(result));
        }
      } catch (err) {
        console.error("push rowNesting_justifyContent page error " + JSON.stringify(err));
        expect().assertFail();
      }
      done();
    });
    afterEach(async function () {
      await CommonFunc.sleep(2000);
      console.info("rowNesting_justifyContent after each called")
    });

    /**
     * @tc.number    SUB_ACE_rowNesting_justifyContent_TEST_0100
     * @tc.name      testColumnSpace
     * @tc.desc      The value of space is set to '10vp'
     */
    it('SUB_ACE_rowNesting_justifyContent_TEST_0100', 0, async function (done) {
      console.info('[SUB_ACE_rowNesting_justifyContent_TEST_0100] START');
      await CommonFunc.sleep(3000);
      console.log('get Initial value')
      let rowNesting_justifyContent_011 = CommonFunc.getComponentRect('rowNesting_justifyContent_011');
      let rowNesting_justifyContent_012 = CommonFunc.getComponentRect('rowNesting_justifyContent_012');
      let rowNesting_justifyContent_013 = CommonFunc.getComponentRect('rowNesting_justifyContent_013');
      let rowNesting_justifyContent_01 = CommonFunc.getComponentRect('rowNesting_justifyContent_01');

      console.log('assert position')
      expect(Math.round(rowNesting_justifyContent_011.top - rowNesting_justifyContent_01.top)).assertEqual(Math.round(rowNesting_justifyContent_01.bottom - rowNesting_justifyContent_011.bottom));
      expect(Math.round(rowNesting_justifyContent_012.top - rowNesting_justifyContent_01.top)).assertEqual(Math.round(rowNesting_justifyContent_01.bottom - rowNesting_justifyContent_012.bottom));
      expect(Math.round(rowNesting_justifyContent_013.top - rowNesting_justifyContent_01.top)).assertEqual(Math.round(rowNesting_justifyContent_01.bottom - rowNesting_justifyContent_013.bottom));

      console.log('assert space')
      expect(Math.round(rowNesting_justifyContent_012.left - rowNesting_justifyContent_011.right)).assertEqual(vp2px(10));
      expect(Math.round(rowNesting_justifyContent_013.left - rowNesting_justifyContent_012.right)).assertEqual(vp2px(10));
      console.log('rowNesting_justifyContent_012.left - rowNesting_justifyContent_011.right', + rowNesting_justifyContent_012.left - rowNesting_justifyContent_011.right)
      console.log('rowNesting_justifyContent_013.left - rowNesting_justifyContent_012.right', + rowNesting_justifyContent_013.left - rowNesting_justifyContent_012.right)


      console.log('assert height')
      expect(Math.round(rowNesting_justifyContent_011.bottom - rowNesting_justifyContent_011.top)).assertEqual(vp2px(50));
      expect(Math.round(rowNesting_justifyContent_012.bottom - rowNesting_justifyContent_012.top)).assertEqual(vp2px(100));
      expect(Math.round(rowNesting_justifyContent_013.bottom - rowNesting_justifyContent_013.top)).assertEqual(vp2px(150));
      console.log('assert weight')
      expect(Math.round(rowNesting_justifyContent_011.right - rowNesting_justifyContent_011.left)).assertEqual(vp2px(50));
      expect(Math.round(rowNesting_justifyContent_012.right - rowNesting_justifyContent_012.left)).assertEqual(vp2px(100));
      expect(Math.round(rowNesting_justifyContent_013.right - rowNesting_justifyContent_013.left)).assertEqual(vp2px(150));
      console.info('[SUB_ACE_rowNesting_justifyContent_TEST_0100] END');
      done();
    });

  })
}